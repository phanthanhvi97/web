import React,{Component} from 'react';

class Product extends Component{
  render(){
    return(
      <div>
        <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <div className="card" style={{width:180}}>
          <img className="card-img-top" src={this.props.image} alt=""/>
          <div className="card-body">
            <h4 className="card-title">{this.props.children}</h4>
            <p className="card-text">{this.props.price}</p>
            <a className="btn btn-primary" href="https://www.w3schools.com/bootstrap4/bootstrap_cards.asp">Mua ngay</a>
          </div>
        </div>
      </div>
      </div>
    )
  }
}

export default Product;
