import React from 'react';
import './App.css';
import Header from './components/Header';
import Product from './components/Product';


function App() {
  
  return (
    <div>
      <Header/>     
      <div className="row">
        <div className="col-lg-4">
          <Product/>
        </div>
        <div className="col-lg-4">
          <Product/>
        </div>
        <div className="col-lg-4">
          <Product/>
        </div>
      </div>      
    </div>
  );
}
export default App;
