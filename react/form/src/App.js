import React, {Component} from 'react';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
       txtName: '',
       txtPassWord: '',
       sltGender:1
    };
  };
  onHandleChange=(event)=>{
      //username:event.target.value
      var target = event.target;
      var name = target.name;
      var value = target.value;
      this.setState({
        [name]:value
      });
      // console.log(target)
  }
  onHandleSubmit=(event)=>{

    //chan su kien submit
    event.preventDefault();
    // console.log(this.state);
  }
  render(){
    return (
      <div className="mt-5">
        <div className="container">
          <div className="col-lg-8">
            <div className="card bg-light mb-3" >
              <div className="card-header">Header</div>
              <div className="card-body">
                <form onSubmit={this.onHandleSubmit}>
                <div className="form-group" >
                  <label>UserName</label>
                  <input type="text"
                    className="form-control" name="txtName" onChange={this.onHandleChange}/>
                </div>
                <div className="form-group" >
                  <label>Pass</label>
                  <input type="password"
                    className="form-control" name="txtPassWord" onChange={this.onHandleChange}/>
                </div>
                <div className="form-group">
                  <select className="form-control" name="sltGender" onChange={this.onHandleChange} value={this.state.sltGender}>
                    <option value={0}>Nu</option>
                    <option value={1}>Nam</option>
                  </select>
                </div>
                    <button type="submit" className="btn btn-primary">Luu</button> &nbsp;
                    <button type="reset" className="btn btn-danger">Xoa</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
  
}

export default App;
