import React,{Component} from 'react';
import './App.css';

class App extends Component{
  constructor(props){
    super(props);
    this.state={
      products: [
        {
          id: 1,
          name: 'iphone 7',
          image: 'https://store.storeimages.cdn-apple.com/8756/as-images.apple.com/is/image/AppleInc/aos/published/images/i/ph/iphone7/plus/iphone7-plus-rosegold-select-2016?wid=940&hei=1112&fmt=png-alpha&qlt=80&.v=1550795424922',
          status: '15000000'
        },
        {
          id: 2,
          name: 'iphone 8',
          image: 'https://store.storeimages.cdn-apple.com/8756/as-images.apple.com/is/image/AppleInc/aos/published/images/i/ph/iphone7/plus/iphone7-plus-rosegold-select-2016?wid=940&hei=1112&fmt=png-alpha&qlt=80&.v=1550795424922',
          status: '15000000'
        }
      ],
      isActive : true
    };
    // this.onSetState=this.onSetState.bind(this);
  }
  onSetState=()=>{
    if(this.state.isActive===true)
      this.setState({
        isActive:false
      });
    else
      this.setState({
        isActive:true
      });
  }
  render(){
    let element = this.state.products.map((product, index)=>{
      let kq='';
      if(product.status){
      kq=
        <tr key={index}>
          <td>{index}</td>
          <td>{product.name}</td>
          <td>
            <span className="badge badge-primary">{product.status}</span>
          </td>
        </tr>
      }
      return kq;
    }      
    )
    return(
      <div>
      <nav className="navbar navbar-expand-sm navbar-light bg-light">
        <a className="navbar-brand" href="http://localhost:3000/">State</a>
        <button className="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId" aria-controls="collapsibleNavId"
            aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>       
      </nav>
      <div className="container">
        <div className="row">
          <table className="table">
            <thead>
              <tr>
                <th>STT</th>
                <th>TENSP</th>
                <th>GIA</th>
              </tr>
            </thead>
            <tbody>
                {element}
            </tbody>
          </table>
          </div>
          <button type="button" className="btn btn-warning" onClick={this.onSetState}>
          {/* <button type="button" className="btn btn-warning" onClick={()=>this.onsetState}> */}
            Active : {this.state.isActive===true?'true':'false'}
          </button>
            
      </div>
      </div>
    )
  }
}


export default App;
