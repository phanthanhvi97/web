var express = require('express')
var app = express()
var server = require('http').Server(app)
var io = require('socket.io')(server)
server.listen(8080, console.log('Da khoi tao server 8080'))

var arr_pos=[]
io.on('connection',(socket)=>{
    io.sockets.emit('server_send_pos', arr_pos)
    console.log('da co client ket noi: '+ socket.id)
    socket.on('client_send_pos',(data)=>{
        arr_pos.push(data);
        console.log(arr_pos)
        io.sockets.emit('server_send_pos', arr_pos)
    })
} )