var express = require('express')
var app=express()

//app.listen(3000, console.log('da khoi tao tai port 3000'))
//client duoc truy cap vao public
app.use(express.static('public'))

//route
app.get('/', function(req, res){
    //ten file ejs
    res.render("trangchu")
})

app.set('view engine', 'ejs')
app.set('views','./views')
//socket io
var server = require('http').Server(app)
server.listen(3000);
var io = require('socket.io')(server)
//doi client ket noi
io.on('connection', function(socket){
    console.log('co nguoi ket noi '+socket.id)
    socket.on('disconnect',()=>{
        console.log(socket.id+ ' ngat ket noi')
    })
    socket.on('Client-send-data',(data)=>{
        console.log(data)
        //server phat ra tat ca client
        io.sockets.emit('Server-send-data',data+'8888')
    })

})



// io.sockets.emit phat tat ca moi nguoi
// socket.emit chi phat lai nguoi gui
//socket.broadcast.emit phat lai nhung nguoi khac
//gui rieng cho nhau
// io.to('socketid').emit()