var express=require('express')
var app=express()
app.use(express.static('public'))
app.set('view engine','ejs')
app.set('views','./views')

var server=require('http').Server(app)
var io = require('socket.io')(server)
server.listen(3000)
app.use(express.static('public'))
app.get('/', (req, res)=>{
    res.render('trangchu')
})
var mangUsers=['aaa']
io.on('connection',(socket)=>{
    console.log('Co nguoi ket noi '+socket.id)
    //sv lang nghe
    socket.on('client-send-Username',(data)=>{
        if(mangUsers.indexOf(data)>=0){
            //fail
            socket.emit('server-send-dangky-thatbai')
        }
        else{
            //ok
            mangUsers.push(data)
            socket.usn=data
            socket.emit('server-send-dangky-thanhcong',data)
            io.sockets.emit('server-send-online',mangUsers)
        }
        }
    )
    socket.on('logout',()=>{
        mangUsers.splice(
            mangUsers.indexOf(socket.usn),1
        )
        socket.broadcast.emit('server-send-online',mangUsers)
    })
    socket.on('user-send-message',(data)=>{
        io.sockets.emit('server-send-message', {ten:socket.usn,nd: data})
    })
})


