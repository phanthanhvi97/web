//1
var mongoose = require('mongoose')
//2 
mongoose.connect('mongodb://localhost/myDatabase', { useNewUrlParser: true })
//3 tao schema
const userSchema = new mongoose.Schema({
    name: String,
    age: Number
})
//4 tao model
//1: collection 2: schema
const user = mongoose.model('user', userSchema)
//5 crud
// user.create([
//     {
//         name: 'teo',
//         age: 20
//     },
//     {
//         name: 'vivuive',
//         age: 22
//     }
// ])
//lay ra
// user.find().exec((err, users)=>{
//     console.log(users);
// })
//update
// user.update({name:'ti'}, {name: 'teo'}).exec((err, result)=>{
//     console.log(result);
    
// })


user.remove({name: 'vivuive'}).exec((err, result)=>{
    console.log(result);
    
})